sap.ui.define([], function () {
	"use strict";

	return {
		buildingBPToView: function (that, inBpData) {

			if (inBpData.generalData.liquorLicenceNumberStartDate) {
				var liquorLicenceNumberStartDate = inBpData.generalData.liquorLicenceNumberStartDate;
				inBpData.generalData.liquorLicenceNumberStartDate = liquorLicenceNumberStartDate.getDate() + "/" + liquorLicenceNumberStartDate.getMonth() +
					"/" + liquorLicenceNumberStartDate.getFullYear();
			}
			if (inBpData.generalData.liquorLicenceNumberEndDate) {
				var liquorLicenceNumberEndDate = inBpData.generalData.liquorLicenceNumberEndDate;
				inBpData.generalData.liquorLicenceNumberEndDate = liquorLicenceNumberEndDate.getDate() + "/" + liquorLicenceNumberEndDate.getMonth() +
					"/" + liquorLicenceNumberEndDate.getFullYear();
			}
			////*******
			if (inBpData.generalData.grocerWineLicenceNumberStartDate) {
				var grocerWineLicenceNumberStartDate = inBpData.generalData.grocerWineLicenceNumberStartDate;
				inBpData.generalData.grocerWineLicenceNumberStartDate = grocerWineLicenceNumberStartDate.getDate() + "/" +
					grocerWineLicenceNumberStartDate.getMonth() +
					"/" + grocerWineLicenceNumberStartDate.getFullYear();
			}
			if (inBpData.generalData.grocerWineLicenceNumberEndDate) {
				var grocerWineLicenceNumberEndDate = inBpData.generalData.grocerWineLicenceNumberEndDate;
				inBpData.generalData.grocerWineLicenceNumberEndDate = grocerWineLicenceNumberEndDate.getDate() + "/" +
					grocerWineLicenceNumberEndDate.getMonth() +
					"/" + grocerWineLicenceNumberEndDate.getFullYear();
			}
			////*********	
			if (inBpData.generalData.distributionLicencNumberStartDate) {
				var distributionLicencNumberStartDate = inBpData.generalData.distributionLicencNumberStartDate;
				inBpData.generalData.distributionLicencNumberStartDate = distributionLicencNumberStartDate.getDate() + "/" +
					distributionLicencNumberStartDate.getMonth() +
					"/" + distributionLicencNumberStartDate.getFullYear();
			}
			if (inBpData.generalData.distributionLicencNumberEndDate) {
				var distributionLicencNumberEndDate = inBpData.generalData.distributionLicencNumberEndDate;
				inBpData.distributionLicencNumberEndDate = distributionLicencNumberEndDate.getDate() + "/" +
					distributionLicencNumberEndDate.getMonth() +
					"/" + distributionLicencNumberEndDate.getFullYear();
			}
			// debugger;
			inBpData.generalData.countryGroupDetail = that.getView().byId("idCountryGroupDetail").getSelectedItem().getProperty("text");
			inBpData.address.provinceSAddress = that.getView().byId("idProvinceSAddress").getSelectedItem().getProperty("text");
			inBpData.address.countrySAddress = that.getView().byId("idCountrySAddress").getSelectedItem().getProperty("text");
			inBpData.address.provincePAddress = that.getView().byId("idProvincePAddress").getSelectedItem().getProperty("text");
			inBpData.address.countryPAddress = that.getView().byId("idCountryPAddress").getSelectedItem().getProperty("text");
			///******Bank*****//
			var inBpDataBank = that.getView().getModel("pModel").getProperty("/bankData");
			if (inBpDataBank) {
				// var arrayBank = (this.getView().byId("idBankDetail").getRows().length) - 1;
				for (var i = 0; i < inBpDataBank.length; i++) {
					inBpData.bankData[i].countryBankDetail = that.getView().byId("idBankDetail").getRows()[i].getCells()[0].getSelectedItem()
						.getProperty("text");
				}
			}
			///***********Company*******
			// debugger;
			var inBpDataCompany = that.getView().getModel("pModel").getProperty("/company");
			// var arrayCompany = (this.getView().byId("idCompanyDetail").getRows().length) - 1;
			if (inBpDataCompany) {
				for (var iCompany = 0; iCompany < inBpDataCompany.length; iCompany++) {
					inBpData.company[iCompany].paymentTerms = that.getView().byId("idCompanyDetail").getRows()[iCompany].getCells()[1].getSelectedItem()
						.getProperty("text");
					inBpData.company[iCompany].paymentMethods = that.getView().byId("idCompanyDetail").getRows()[iCompany].getCells()[2]
						.getSelectedItem().getProperty("text");
				}
			}
			////////******************************************************************//////
		},
		buildingBPObjectMapping: function (data) {
			// debugger;
			// var data = {};
			// =======
			// 		buildingBPObjectMapping: function () {
			// 			debugger;
			// 			var data = {};
			// >>>>>>> 330c09c Field validation for following sections 1.General detail 2.Address Detail
			var respData = {};
			respData.generalData = {};
			respData.contacts = {};
			respData.address = {};
			respData.bpData = [];
			///********************************************
			///converting mandatory fields to upper case
			data.generalData.nameGroupDetail.toUpperCase();
			data.generalData.entityRegistrationNumber.toUpperCase();
			data.address.streetNameSAddress.toUpperCase();
			data.address.citySAddress.toUpperCase();
			data.address.postalCodeSAddress.toUpperCase();
			///********************************************
			var BusinessPartner = data.generalData.code;
			//********************temp testing8**********************////
			//////////////////////////////////////////////////////////////
			// var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			// oStorage.put("myLocalData", data);
			// oStorage.get("myLocalData");
			// oStorage.clear();

			////////________________________________________________//////
			var OrganizationBPName1 = data.generalData.nameGroupDetail;
			var SearchTerm1 = data.generalData.searchTerm1;
			var BusinessPartnerIsBlocked = false;
			var BusinessPartnerCategory = "2";
			var BusinessPartnerGrouping = data.generalData.group;
			// var BPIdentificationType_1 = "Z001";
			// var BPIdentificationType_2 = "Z002";
			// var BPIdentificationType_3 = "Z003";
			// var BPIdentificationNumber = "ABVKD93498";
			////**********************Start of Address data**************/////

			var complexNumberSAddress =
				function () {
					if (data.address.complexNumberSAddress)
						return data.address.complexNumberSAddress;
					else return "";
				};
			var streetNumberSAddress = data.address.streetNumberSAddress;
			var streetNameSAddress = data.address.streetNameSAddress;
			var subrubSAddress = data.address.subrubSAddress;
			var citySAddress = data.address.citySAddress;
			var postalCodeSAddress = data.address.postalCodeSAddress;
			var provinceSAddres = data.address.provinceSAddres;
			var countrySAddress = data.address.countrySAddress;
			var PrfrdCommMediumType = "INT";
			var StreetPrefixName = "";

			var addressTypePAddress = data.address.addressTypePAddress;
			var addressNumberPAddress = data.address.addressNumberPAddress;
			var cityPAddress = data.address.cityPAddress;
			var postalCodePAddress = data.addresspostalCodePAddress;
			var provincePAddress = data.address.provincePAddress;
			var countryPAddress = data.address.countryPAddress;
			var AddressUsage = "XXDEFAULT";
			//////*********************End of Address Data*****************/////

			//////*********************Start of Contact Data*****************/////

			///**********Creating Telephone Object-to_PhoneNumber***********///

			// var telephoneArray = [];

			var to_PhoneNumber = [];
			var to_FaxNumber = [];
			var to_EmailAddress = [];

			if (data.contacts) {
				var iPhone = 0;
				var telephoneArray = {};
				if (data.contacts.telephoneData) {
					telephoneArray = data.contacts.telephoneData;
					for (; iPhone < telephoneArray.length; iPhone++) {
						to_PhoneNumber[iPhone] = {};
						to_PhoneNumber[iPhone].IsDefaultPhoneNumber = telephoneArray[iPhone].primary;
						to_PhoneNumber[iPhone].PhoneNumber = telephoneArray[iPhone].telephoneNo;
						to_PhoneNumber[iPhone].Person = telephoneArray[iPhone].telephoneName;
						to_PhoneNumber[iPhone].PhoneNumberExtension = telephoneArray[iPhone].telephoneExtension;
						to_PhoneNumber[iPhone].InternationalPhoneNumber = "+" + telephoneArray[iPhone].telephoneDialingCode + "" + telephoneArray[iPhone]
							.telephoneNo;
						to_PhoneNumber[iPhone].PhoneNumberType = telephoneArray[iPhone].idTelephoneTabContactType;
						to_PhoneNumber[iPhone].OrdinalNumber = iPhone.toString();
					}
				}
				///**********Creating Cellphone Object-to_PhoneNumber***********///
				////------------------------------------------------------------////
				if (data.contacts.cellphoneData) {
					var cellphoneArray = data.contacts.cellphoneData;
					var cPhone = 0;
					var totalLength = 0;
					// if (telephoneArray) {
					totalLength = telephoneArray.length + cellphoneArray.length;
					// } else {
					// 	totalLength = cellphoneArray.length;
					// }

					for (; iPhone < totalLength; iPhone++) {
						to_PhoneNumber[iPhone] = {};
						to_PhoneNumber[iPhone].IsDefaultPhoneNumber = cellphoneArray[cPhone].primary;
						to_PhoneNumber[iPhone].Person = cellphoneArray[cPhone].cellphoneName;
						to_PhoneNumber[iPhone].PhoneNumber = cellphoneArray[cPhone].cellphoneNo;
						to_PhoneNumber[iPhone].InternationalPhoneNumber = "+" + cellphoneArray[cPhone].cellphoneDialingCode + "" + cellphoneArray[cPhone]
							.cellphoneNo;
						to_PhoneNumber[iPhone].PhoneNumberType = cellphoneArray[cPhone].cellphoneContactType;
						to_PhoneNumber[iPhone].OrdinalNumber = iPhone.toString();
					}
				}
				///**********Creating Fax Object-to_FaxNumber***********///
				////------------------------------------------------------------////
				if (data.contacts.faxData) {
					var faxArray = data.contacts.faxData;
					for (var iFax = 0; iFax < faxArray.length; iFax++) {
						to_FaxNumber[iFax] = {};
						to_FaxNumber[iFax].IsDefaultFaxNumber = faxArray[iFax].primary;
						to_FaxNumber[iFax].FaxNumber = faxArray[iFax].faxNo;
						to_FaxNumber[iFax].Person = faxArray[iFax].faxName;
						to_FaxNumber[iFax].InternationalFaxNumber = "+" + faxArray[iFax].faxDialingCode + "" + faxArray[iFax].faxNo;
						to_FaxNumber[iFax].OrdinalNumber = iFax.toString();
					}
				}
				///**********Creating Eail Object-to_EmailAddress***********///
				////------------------------------------------------------------////
				if (data.contacts.emailData) {
					var emailArray = data.contacts.emailData;
					for (var iEmail = 0; iEmail < emailArray.length; iEmail++) {
						to_EmailAddress[iEmail] = {};
						to_EmailAddress[iEmail].IsDefaultEmailAddress = emailArray[iEmail].primary;
						to_EmailAddress[iEmail].Person = emailArray[iEmail].emailName;
						to_EmailAddress[iEmail].EmailAddress = emailArray[iEmail].emailId;
						to_EmailAddress[iEmail].SearchEmailAddress = emailArray[iEmail].emailId;
						to_EmailAddress[iEmail].OrdinalNumber = iEmail.toString();
					}
				}
			}
			///////*****************************Bank Detail Section**********************///
			/////___________________________________________________________________///////

			var to_BusinessPartnerBank = [];
			var bankData = data.bankData;
			if (bankData) {
				for (var iBank = 0; iBank < bankData.length; iBank++) {
					to_BusinessPartnerBank[iBank] = {};
					to_BusinessPartnerBank[iBank].BusinessPartner = BusinessPartner;
					var bankId = iBank.toString();
					if (bankId.length === 1) {
						bankId = "000" + bankId;
					} else if (bankId.length === 2) {
						bankId = "00" + bankId;
					}
					to_BusinessPartnerBank[iBank].BankIdentification = bankId; //"0001"; //iBank;
					to_BusinessPartnerBank[iBank].BankCountryKey = bankData[iBank].countryBankDetail;
					to_BusinessPartnerBank[iBank].BankName = bankData[iBank].bankNameBank;
					to_BusinessPartnerBank[iBank].BankNumber = bankData[iBank].bankCodeBank;
					to_BusinessPartnerBank[iBank].BankAccountHolderName = bankData[iBank].accountNameBank;
					to_BusinessPartnerBank[iBank].BankAccount = bankData[iBank].accountNumberBank;
					to_BusinessPartnerBank[iBank].CollectionAuthInd = true;
				}
			}
			///**************Business Partner Identification - to_BuPaIdentification*************////
			////_______________________________________________________________________/////

			var to_BuPaIdentification = [];

			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "yyyy-MM-dd"
			});

			//*********Store Liquor License
			if (data.generalData.distributionLicencNumber) {
				to_BuPaIdentification[0] = {};
				to_BuPaIdentification[0].BusinessPartner = BusinessPartner;
				to_BuPaIdentification[0].BPIdentificationType = "Z001";
				to_BuPaIdentification[0].BPIdentificationNumber = data.generalData.liquorLicenceNumber;
				to_BuPaIdentification[0].BPIdnNmbrIssuingInstitute = "";

				// var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				// 	pattern: "yyyy-MM-dd"
				// });
				var liquorLicenceEntryDate = dateFormat.format(new Date());
				liquorLicenceEntryDate = liquorLicenceEntryDate + "T00:00:00";
				to_BuPaIdentification[0].BPIdentificationEntryDate = dateFormat.format(new Date()) + "T00:00:00"; //liquorLicenceEntryDate; //new Date("24.05.2020");
				// debugger;
				// var vStartDate = data.liquorLicenceNumberStartDate.getDate() + "." + data.liquorLicenceNumberStartDate.getMonth() + "." + data.generalData.liquorLicenceNumberStartDate
				// 	.getFullYear();
				var liquorLicenceStartDate = dateFormat.format(data.generalData.liquorLicenceNumberStartDate);
				liquorLicenceStartDate = liquorLicenceStartDate + "T00:00:00";
				to_BuPaIdentification[0].ValidityStartDate = dateFormat.format(new Date()) + "T00:00:00"; //liquorLicenceStartDate; //new Date(liquorLicenceStartDate); //data.generalData.liquorLicenceNumberStartDate;

				to_BuPaIdentification[0].ValidityEndDate = dateFormat.format(new Date()) + "T00:00:00"; //new Date("17.05.2020"); //data.generalData.liquorLicenceNumberEndDate;
				to_BuPaIdentification[0].Country = "";
				to_BuPaIdentification[0].Region = "";
				to_BuPaIdentification[0].AuthorizationGroup = "";
			}
			//*********Grocer's Wine License
			if (data.generalData.grocerWineLicenceNumber) {
				to_BuPaIdentification[1] = {};
				to_BuPaIdentification[1].BusinessPartner = BusinessPartner;
				to_BuPaIdentification[1].BPIdentificationType = "Z002";
				to_BuPaIdentification[1].BPIdentificationNumber = data.generalData.grocerWineLicenceNumber;
				to_BuPaIdentification[1].BPIdnNmbrIssuingInstitute = "";
				to_BuPaIdentification[1].BPIdentificationEntryDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020";
				// to_BuPaIdentification[1].BPIdentificationEntryDate = new Date();
				// data.grocerWineLicenceNumberStartDate
				// var wineLicenceStartDate = dateFormat.format(new Date()) + "T00:00:00";
				to_BuPaIdentification[1].ValidityStartDate = dateFormat.format(new Date()) + "T00:00:00"; //wineLicenceStartDate; //new Date(wineLicenceStartDate); //oDateFormat.format(data.generalData.grocerWineLicenceNumberStartDate);
				to_BuPaIdentification[1].ValidityEndDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020"; //data.generalData.grocerWineLicenceNumberEndDate;
				to_BuPaIdentification[1].Country = "";
				to_BuPaIdentification[1].Region = "";
				to_BuPaIdentification[1].AuthorizationGroup = "";
			}
			//*********Distribution License
			if (data.generalData.distributionLicencNumber) {
				to_BuPaIdentification[2] = {};
				to_BuPaIdentification[2].BusinessPartner = BusinessPartner;
				to_BuPaIdentification[2].BPIdentificationType = "Z003";
				to_BuPaIdentification[2].BPIdentificationNumber = data.generalData.distributionLicencNumber;
				to_BuPaIdentification[2].BPIdnNmbrIssuingInstitute = "";
				to_BuPaIdentification[2].BPIdentificationEntryDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020";
				// // to_BuPaIdentification[2].BPIdentificationEntryDate = new Date();
				to_BuPaIdentification[2].ValidityStartDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020"; //data.generalData.distributionLicencNumberStartDate;
				to_BuPaIdentification[2].ValidityEndDate = dateFormat.format(new Date()) + "T00:00:00"; //"24.05.2020"; //data.generalData.distributionLicencNumberEndDate;
				to_BuPaIdentification[2].Country = "";
				to_BuPaIdentification[2].Region = "";
				to_BuPaIdentification[2].AuthorizationGroup = "";
			}

			// if (data.)
			///**************Business Partner Role - to_BusinessPartnerRole*************////
			////_______________________________________________________________________/////
			var to_BusinessPartnerRole = [{
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "BPSITE"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "ZFLCU0"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "ZFLCU1"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "ZFLVN0"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "ZFLVN1"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "FLCU00"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "FLVN01"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "FLCU01"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "FLVN00"
			}, {
				BusinessPartner: BusinessPartner,
				BusinessPartnerRole: "UKM000"
			}];

			///**************Business Partner Contact - to_BusinessPartnerContact*************////
			////_______________________________________________________________________/////
			// var to_BusinessPartnerContact = [];

			///**************Business Partner Tax - to_BusinessPartnerTax*************////
			////_______________________________________________________________________/////			
			var to_BusinessPartnerTax = [];
			var bpTaxCount = 0;
			if (data.generalData.vatRegistrationNo) {
				// bpTaxCount++;
				to_BusinessPartnerTax[bpTaxCount] = {};
				to_BusinessPartnerTax[bpTaxCount].BusinessPartner = BusinessPartner;
				to_BusinessPartnerTax[bpTaxCount].BPTaxType = "ZA1";
				to_BusinessPartnerTax[bpTaxCount].BPTaxNumber = data.generalData.vatRegistrationNo;
			}
			if (data.generalData.entityRegistrationNumber) {
				bpTaxCount++;
				to_BusinessPartnerTax[bpTaxCount] = {};
				to_BusinessPartnerTax[bpTaxCount].BusinessPartner = BusinessPartner;
				to_BusinessPartnerTax[bpTaxCount].BPTaxType = "ZA4";
				to_BusinessPartnerTax[bpTaxCount].BPTaxNumber = data.generalData.entityRegistrationNumber;
			}
			////------------Customer Object - to_Customer--------------------////
			////_______________Customer Company Variable______________//////
			// debugger;
			var to_CustomerCompany = [];
			var to_CustomerSalesArea = [];
			var Company = data.company;
			if (Company) {
				for (var cCount = 0; cCount < Company.length; cCount++) {
					////*********Coding to create the company
					to_CustomerCompany[cCount] = {};
					to_CustomerCompany[cCount].Customer = BusinessPartner;
					to_CustomerCompany[cCount].CompanyCode = Company[cCount].companyCode;
					to_CustomerCompany[cCount].AccountingClerkInternetAddress = Company[cCount].accountingClerkEmails;
					to_CustomerCompany[cCount].CustomerAccountNote = "PAYMENT REF STORE";
					to_CustomerCompany[cCount].CustomerSupplierClearingIsUsed = false;
					to_CustomerCompany[cCount].InterestCalculationCode = "Z1";
					to_CustomerCompany[cCount].IntrstCalcFrequencyInMonths = "0";
					to_CustomerCompany[cCount].IsToBeLocallyProcessed = false;
					to_CustomerCompany[cCount].ItemIsToBePaidSeparately = false;
					to_CustomerCompany[cCount].LayoutSortingRule = "001";
					to_CustomerCompany[cCount].PaymentMethodsList = Company[cCount].paymentMethods;
					to_CustomerCompany[cCount].PaymentTerms = Company[cCount].paymentTerms;
					to_CustomerCompany[cCount].PaytAdviceIsSentbyEDI = false;
					to_CustomerCompany[cCount].PhysicalInventoryBlockInd = false;
					to_CustomerCompany[cCount].ReconciliationAccount = Company[cCount].reconciliationAccount;
					to_CustomerCompany[cCount].RecordPaymentHistoryIndicator = true;
					// to_CustomerCompany[cCount].UserAtCustomer = "";
					to_CustomerCompany[cCount].DeletionIndicator = false;

					// if(to_CustomerCompany[cCount].CompanyCode === Managing DC){
					// to_CustomerCompany[cCount].to_CustomerDunning = {};
					// to_CustomerCompany[cCount].to_WithHoldingTax = {};
					// }
					// else{
					to_CustomerCompany[cCount].to_CustomerDunning = [];
					to_CustomerCompany[cCount].to_WithHoldingTax = [];
					// }
					////***********Coding to create the sales area
					to_CustomerSalesArea[cCount] = {};
					to_CustomerSalesArea[cCount].Customer = BusinessPartner;
					to_CustomerSalesArea[cCount].SalesOrganization = Company[cCount].companyCode;
					to_CustomerSalesArea[cCount].DistributionChannel = "95";
					to_CustomerSalesArea[cCount].Division = "01";
					to_CustomerSalesArea[cCount].CompleteDeliveryIsDefined = false;
					to_CustomerSalesArea[cCount].Currency = "ZAR";
					to_CustomerSalesArea[cCount].CustomerPaymentTerms = Company[cCount].paymentTerms;
					to_CustomerSalesArea[cCount].CustomerPricingProcedure = "1";
					to_CustomerSalesArea[cCount].DeliveryPriority = "2";
					to_CustomerSalesArea[cCount].DeletionIndicator = false;
					to_CustomerSalesArea[cCount].ItemOrderProbabilityInPercent = "0";
					to_CustomerSalesArea[cCount].OrderCombinationIsAllowed = true;
					to_CustomerSalesArea[cCount].ShippingCondition = "01";
					///Codeing for the partner function in sales area
					to_CustomerSalesArea[cCount].to_PartnerFunction = [];
					for (var sCount = 0; sCount < 4; sCount++) {
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount] = {};
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].Customer = BusinessPartner;
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].SalesOrganization = Company[cCount].companyCode;
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].DistributionChannel = "95";
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].Division = "01";
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].PartnerCounter = "0";
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].BPCustomerNumber = BusinessPartner;
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].CustomerPartnerDescription = "";
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].DefaultPartner = false;
						to_CustomerSalesArea[cCount].to_PartnerFunction[sCount].AuthorizationGroup = "";
					}
					to_CustomerSalesArea[cCount].to_PartnerFunction[0].PartnerFunction = "SP";
					to_CustomerSalesArea[cCount].to_PartnerFunction[1].PartnerFunction = "BP";
					to_CustomerSalesArea[cCount].to_PartnerFunction[2].PartnerFunction = "PY";
					to_CustomerSalesArea[cCount].to_PartnerFunction[3].PartnerFunction = "SH";

					//Codeing for the sales tax area in sales area	
					to_CustomerSalesArea[cCount].to_SalesAreaTax = [];
					for (var taxCount = 0; taxCount < 5; taxCount++) {
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount] = {};
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].Customer = BusinessPartner;
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].SalesOrganization = Company[cCount].companyCode;
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].DistributionChannel = "95";
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].Division = "01";
						to_CustomerSalesArea[cCount].to_SalesAreaTax[taxCount].CustomerTaxCategory = "MWST";

					}
					to_CustomerSalesArea[cCount].to_SalesAreaTax[0].DepartureCountry = "SG";
					to_CustomerSalesArea[cCount].to_SalesAreaTax[1].DepartureCountry = "MZ";
					to_CustomerSalesArea[cCount].to_SalesAreaTax[2].DepartureCountry = "NA";
					to_CustomerSalesArea[cCount].to_SalesAreaTax[3].DepartureCountry = "ZA";
					to_CustomerSalesArea[cCount].to_SalesAreaTax[4].DepartureCountry = "BW";
				}
			}
			// var LayoutSortingRule = "001";
			// var RecordPaymentHistoryIndicator = true;
			// "Customer": "80314",
			// "CompanyCode": "1061",
			// "AccountingClerk": "",
			// "AccountByCustomer": "",
			// "LayoutSortingRule": "001",
			// "ReconciliationAccount": "140000",
			// "PaymentTerms": "Z30D",
			// "PaymentMethodsList": "D",
			//"RecordPaymentHistoryIndicator": true,
			// "UserAtCustomer": "CLERK 1",
			// "AccountingClerkInternetAddress": "manager@knowlesops.co.za"
			/////___________________________________________________//////
			//////_______________Customer Sales Area________________/////
			// var CustomerSalesAreaPartnerFunction = [];
			// var CustomerSalesAreaTax = [];
			// var CustomerSalesArea = [];
			//////__________________________________________________//////
			var to_Customer = {};
			to_Customer.Customer = BusinessPartner;
			to_Customer.DeletionIndicator = false;
			to_Customer.CustomerAccountGroup = data.generalData.group;
			to_Customer.CustomerFullName = OrganizationBPName1;
			to_Customer.CustomerName = OrganizationBPName1;
			to_Customer.PostingIsBlocked = false;
			to_Customer.InternationalLocationNumber1 = "0";
			to_Customer.TaxNumber1 = data.generalData.vatRegistrationNo;
			to_Customer.TaxNumber3 = data.generalData.entityRegistrationNumber;
			to_Customer.DeletionIndicator = false;
			to_Customer.to_CustomerCompany = to_CustomerCompany;
			to_Customer.to_CustomerSalesArea = [];
			// to_CustomerSalesArea;

			var request = {
				BusinessPartner: BusinessPartner,
				Customer: BusinessPartner,
				Supplier: BusinessPartner,
				BusinessPartnerCategory: BusinessPartnerCategory,
				BusinessPartnerFullName: OrganizationBPName1,
				BusinessPartnerGrouping: BusinessPartnerGrouping,
				BusinessPartnerName: OrganizationBPName1,
				InternationalLocationNumber1: "0",
				InternationalLocationNumber2: "0",
				IsFemale: false,
				IsMale: false,
				IsSexUnknown: false,
				OrganizationBPName1: OrganizationBPName1,
				OrganizationBPName4: OrganizationBPName1,
				SearchTerm1: SearchTerm1,
				BusinessPartnerIsBlocked: BusinessPartnerIsBlocked,
				BusinessPartnerType: "0002",
				InternationalLocationNumber3: "0",
				IsMarkedForArchiving: false,
				to_BuPaIdentification: to_BuPaIdentification,

				to_BusinessPartnerAddress: [{
					BusinessPartner: BusinessPartner,
					CityName: citySAddress,
					Country: countrySAddress,
					District: subrubSAddress,
					FullName: OrganizationBPName1,
					HouseNumber: complexNumberSAddress,
					Language: "EN",
					PostalCode: postalCodeSAddress,
					PrfrdCommMediumType: PrfrdCommMediumType,
					Region: provinceSAddres,
					StreetName: streetNameSAddress,
					StreetPrefixName: StreetPrefixName,
					////**********Postal Address*******///
					DeliveryServiceNumber: addressNumberPAddress,
					DeliveryServiceTypeCode: "", //addressTypePAddress,
					POBoxPostalCode: postalCodePAddress,
					POBoxDeviatingCityName: cityPAddress,
					POBoxDeviatingRegion: provincePAddress,
					POBoxDeviatingCountry: countryPAddress,
					to_AddressUsage: [{
						BusinessPartner: BusinessPartner,
						//ValidityEndDate:"9999-01-01T00:00:00Z",
						AddressUsage: AddressUsage
							//ValidityStartDate":"2018-09-21T15:20:15Z"
					}],

					to_PhoneNumber: to_PhoneNumber,
					to_FaxNumber: to_FaxNumber,
					to_EmailAddress: to_EmailAddress
				}],
				to_BusinessPartnerBank: to_BusinessPartnerBank,
				to_BusinessPartnerRole: to_BusinessPartnerRole,
				// to_BusinessPartnerContact: to_BusinessPartnerContact,
				to_BusinessPartnerTax: to_BusinessPartnerTax,
				to_Customer: to_Customer
			};

			return request;
		}
	};
});