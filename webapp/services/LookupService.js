sap.ui.define([], function () {
	"use strict";

	function _callGetService(targetURL, options) {
		return fetch(targetURL, options)
			.then(function (res) {
				if (res.ok) {
					if (res.headers.get("X-CSRF-Token")) {
						return res;
					} else {
						return res.json();
					}
				} else {
					return Promise.reject(res.status);
				}
			});
	}

	return {

		getCountryList: function () {
			return _callGetService("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/CountryCodeSet?$format=json");
		},
		getCountryCode: function (countryCode) {
			return _callGetService("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/DialingCodeSet?$filter=countryCode eq '" + countryCode +
				"' &$format=json");
		},
		getProvinceCode: function (countryCode) {
			return _callGetService("/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/ProvinceSet?$filter=countryCode eq '" + countryCode +
				"' &$format=json");
		},
		getDialingCode: function (countryCode) {
			return _callGetService("/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/DialingCodeSet?$filter=countryCode eq '" + countryCode +
				"' &$format=json");
		},
		getStoreFormatList: function () {
			return _callGetService("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/StoreFormatSet?$format=json");
		},
		getAccountClerkList: function () {
			return _callGetService("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/AccountClerkSet?$format=json");
		},
		getCorporateStoreIndicatorList: function () {
			return _callGetService("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/CorporateStoreSet?$format=json");
		},
		getStoreOpenReasonList: function () {
			return _callGetService("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/StoreOpenReasonSet?$format=json");
		},
		getBankNameList: function () {
			return _callGetService("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/BankNameSet?$filter=countryCode eq 'ZA' &$format=json");
		},
		getBEEClassificationList: function () {
			return _callGetService("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/BEEClassificationSet?$format=json");
		},
		getBEELevelList: function (beeCatCode) {
			// debugger;
			return _callGetService("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/BeeLevelSet?$filter=beeCatCode eq '" + beeCatCode +
				"' &$format=json");
		},
		getBEECategoryList: function () {
			return _callGetService("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/BeeCategorySet?$format=json");
		},
		getPaymentMethodsList: function (countryCode) {
			return _callGetService("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/PaymentMethodsSet?$filter=country eq '" + countryCode +
				"'&$format=json");
		},
		getPaymentTermsList: function () {
			return _callGetService("/FS1/sap/opu/odata/sap/ZBP_MDM_LOOKUPS_SRV/PaymentTermsSet?$filter=language eq 'EN'&$format=json");
		},
		getCompanyCodeList: function () {
			return _callGetService(
				"/FS1/sap/opu/odata/sap/FAC_MANAGE_GLACCOUNT_SRV/AssignedCompanyCode_VHSet?$ChartOfAccounts eq 'SPAR' &$format=json");
		},
		getBusinessPartnerView: function (filterURL) {
			var targetURL = "/FS1/sap/opu/odata/sap/API_BUSINESS_PARTNER/" +
				filterURL;
			return _callGetService(targetURL);
		}

	};
});