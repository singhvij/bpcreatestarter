sap.ui.define([], function () {
	"use strict";

	return {
		checkInputConstraints: function (that, fieldIds, isTableArray) {
			var validationFlag = true;
			////*******check if the control type is string**************///
			for (var i = 0; i < fieldIds.length; i++) {
				// var controlId = ;
				if (isTableArray) {
					var valid = this.validateOnAddInArrayFields(that, fieldIds[i]);
					if (!valid) {
						var oPath = that.getView().byId(fieldIds[i].getId()).getBinding().sPath;
						var str = "";
						if (oPath.lastIndexOf("/") === 0) {
							str = (oPath).substring(1, (oPath).length);
						} else {
							str = (oPath).slice(1, (oPath).substring(1, (oPath).length).indexOf("/") + 1);
						}
						var arrayIndex = that.getView().getModel("validationModel").getProperty("/").indexOf(str);
						if (arrayIndex === -1) {
							that.getView().getModel("validationModel").getProperty("/").push(str);
						}
						// that.getView().getModel("validationModel").setProperty("/", (oPath).slice(1, (oPath).substring(1, (oPath).length).indexOf("/") + 1));
					}
					if (validationFlag) {
						validationFlag = valid;
					}
				} else {
					valid = this.validateField(that, fieldIds[i]);
					if (!valid) {
						var oPath1 = that.getView().byId(fieldIds[i].getId()).getBinding("value").sPath;
						var str1 = (oPath1).slice(1, (oPath1).substring(1, (oPath1).length).indexOf("/") + 1);
						var arrayIndex1 = that.getView().getModel("validationModel").getProperty("/").indexOf(str1);
						if (arrayIndex1 === -1) {
							that.getView().getModel("validationModel").getProperty("/").push(str1);
						}
						// var oPath1 = that.getView().byId(fieldIds[i].getId()).getBinding("value").sPath;
						// that.getView().getModel("validationModel").getProperty("/").push((oPath1).slice(1, (oPath1).substring(1, (oPath1).length).indexOf(
						// 	"/") + 1));
						// that.getView().getModel("validationModel").setProperty("/", (oPath1).slice(1, (oPath1).substring(1, (oPath1).length).indexOf("/") +1));
					}
					if (validationFlag) {
						validationFlag = valid;
					}
				}
			}
			return validationFlag;
		},

		validateField: function (that, cellInfo) {
			var valid = true;
			var message = "";
			if (cellInfo.getMetadata().getName() === "sap.m.Input") {
				var fieldId = cellInfo.getId();
				if (that.getView().byId(fieldId).getBinding("value").getType()) {
					if (that.getView().byId(fieldId).getBinding("value").getType().sName === "String") {
						var value = that.getView().byId(fieldId).getValue();
						var minLength = that.getView().byId(fieldId).getBinding("value").getType().oConstraints.minLength;
						var maxLength = that.getView().byId(fieldId).getBinding("value").getType().oConstraints.maxLength;
						var regEx = that.getView().byId(fieldId).getBinding("value").getType().oConstraints.search;
						if (value.length < minLength) {
							valid = false;
							message = "Enter a value with at least " + minLength + " characters";
							this._setControlValueState(that, fieldId, message, sap.ui.core.ValueState.Error);
						} else if (value.length > maxLength) {
							valid = false;
							message = "Enter a value with no more than " + maxLength + " characters";
							this._setControlValueState(that, fieldId, message, sap.ui.core.ValueState.Error);
						} else {
							if (!value.match(regEx)) {
								valid = false;
								message = "Enter a valid value. ";
								this._setControlValueState(that, fieldId, message, sap.ui.core.ValueState.Error);
							} else {
								valid = true;
								message = "";
								this._setControlValueState(that, fieldId, message, sap.ui.core.ValueState.None);
							}
						}
					}
				}
			}
			return valid;
		},
		_setControlValueState: function (that, fieldId, message, status) {
			that.getView().byId(fieldId).setValueStateText(message);
			that.getView().byId(fieldId).setValueState(status);
		},
		_setArrayControlValueState: function (oRow, controlBindingName, message, status) {
			oRow.validFields[controlBindingName] = status;
			oRow.validationMsg[controlBindingName] = message;
		},
		validateOnAddInArrayFields: function (that, tableInfo) {
			// debugger;
			// var cellInfo = oEvent.getSource().getParent().getParent();
			var controlId = tableInfo.getId();
			var tableArray = that.getView().getModel("pModel").getProperty(that.getView().byId(controlId).getBinding().sPath);
			var columnList = 0;
			var validationFlag = true;

			if (tableArray) {
				var tALength = tableArray.length;
				if (tALength > 0) {
					that.getView().byId(controlId).setFirstVisibleRow(0);

					columnList = that.getView().byId(controlId).getColumns();
					// if (tALength > 3) {
					// 	debugger;
					// 	that.getView().byId(controlId).setFirstVisibleRow(tALength + 1);
					tALength = 1; // this is temp as we are adding to the top of hte array
					// }
					for (var i = 0; i < columnList.length; i++) {
						var cellDetail = that.getView().byId(controlId).getRows()[tALength - 1].getCells()[i];
						var controlRendererName = that.getView().byId(controlId).getRows()[tALength - 1].getCells()[i].getMetadata().getRendererName();
						var valid = true;
						if (controlRendererName === "sap.m.InputRenderer") {
							// var controlBindingName = that.getView().byId(controlId).getRows()[tALength - 1].getCells()[i].getBindingInfo("value").binding.sPath;
							// var value = that.getView().getModel("pModel").getProperty(cellInfo.getBinding().sPath)[0][controlBindingName];
							valid = this.validateOnAddInArrayField(that, cellDetail, i);
						}
						if (validationFlag) {
							validationFlag = valid;
						}
					}
				}
			}
			that.getView().byId(tableInfo.getId()).getModel("pModel").refresh();
			return validationFlag;
		},
		validateOnAddInArrayField: function (that, cellInfo, cellNo) {
				var valid = true;
				var message = "";
				var pModel = "pModel";
				if (cellInfo.getMetadata().getName() === "sap.m.Input") {
					var fieldId = cellInfo.getId();
					if (that.getView().byId(fieldId).getBinding("value").getType()) {
						if (that.getView().byId(fieldId).getBinding("value").getType().sName === "String") {
							// var value = that.getView().byId(fieldId).getValue();
							var minLength = that.getView().byId(fieldId).getBinding("value").getType().oConstraints.minLength;
							var maxLength = that.getView().byId(fieldId).getBinding("value").getType().oConstraints.maxLength;
							var regEx = that.getView().byId(fieldId).getBinding("value").getType().oConstraints.search;
							var controlBindingName = that.getView().byId(cellInfo.getParent().getParent().getId()).getRows()[0].getCells()[cellNo].getBindingInfo(
								"value").binding.sPath;
							var oRow = that.getView().getModel(pModel).getProperty(cellInfo.getParent().getParent().getBinding().sPath)[0];
							var value = oRow[controlBindingName];
							if (value.length < minLength) {
								valid = false;
								message = "Enter a value with at least " + minLength + " characters";
								this._setArrayControlValueState(oRow, controlBindingName, message, "Error");
							} else if (value.length > maxLength) {
								valid = false;
								message = "Enter a value with no more than " + maxLength + " characters";
								this._setArrayControlValueState(oRow, controlBindingName, message, "Error");
							} else {
								if (!value.match(regEx)) {
									valid = false;
									message = "Enter a valid value. ";
									this._setArrayControlValueState(oRow, controlBindingName, message, "Error");
								} else {
									valid = true;
									message = "";
									this._setArrayControlValueState(oRow, controlBindingName, message, "None");
								}
							}
						}
					}
				}
				return valid;
			}
			// validEmailRegEx: function (oEvent, email) {
			// 	var mailregex = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/;
			// 	if (!email.match(mailregex)) {
			// 		var message = "Enter a valid value";
			// 		oEvent.getSource().setValueStateText(message);
			// 		oEvent.getSource().setValueState(sap.ui.core.ValueState.Error);
			// 	} else {
			// 		// this.removeMessageFromTarget(sTarget);
			// 		oEvent.getSource().setValueStateText("");
			// 		oEvent.getSource().setValueState(sap.ui.core.ValueState.None);
			// 		// rInput.setValueState("None");
			// 	}
			// }
	};
});