/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"spar/bp/create/starter/BPCreate_Starter/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});