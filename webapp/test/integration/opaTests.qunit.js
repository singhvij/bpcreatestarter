/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"spar/bp/create/starter/BPCreate_Starter/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});