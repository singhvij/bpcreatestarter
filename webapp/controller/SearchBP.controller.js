sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"spar/bp/create/starter/BPCreate_Starter/services/BusinessPartnerService",
	"spar/bp/create/starter/BPCreate_Starter/services/LookupService"
], function (Controller, JSONModel, BPService, LookupService) {
	"use strict";

	return Controller.extend("spar.bp.create.starter.BPCreate_Starter.controller.SearchBP", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.SearchBP
		 */
		onInit: function () {
			this.getView().setModel(new JSONModel({
				"filterData": {}
			}), "filterModel");
			// var res = BPService.getBusinessPartner();
			// LookupService.getBusinessPartnerView("100").then(function (res) {
			// 	// debugger;
			// 	var storeFormatModel = new JSONModel(res.d);
			// 	storeFormatModel.setSizeLimit(res.d.results.length);
			// 	this.getOwnerComponent().setModel(storeFormatModel, "storeFormatModel");
			// });
		},
		onPressTableRow: function (oEvent) {
			debugger;
			// var selectedIndex = oEvent.getSource().getItemNavigation().iFocusedIndex - 1;
			var businessPartner = this.getView().getModel("BPSearchResultModel").getProperty("/")[oEvent.getSource().getItemNavigation().iFocusedIndex -
				1].BusinessPartner;

			// var targetURL = "A_BusinessPartner?$format=json&$expand=to_BusinessPartnerTax&$top=" + recordCount + "&$filter=(" +
			// 	this._createURL(urlArray, " or ") + ")";
			// LookupService.getBusinessPartnerView(targetURL)
			// 	.then(function (resp) {
			// 		finalResult = resp.d;
			// var BPSearchResultModel = new JSONModel(finalResult);
			// BPSearchResultModel.setSizeLimit(recordCount); //searchResult.results.length);
			// that.getOwnerComponent().setModel(BPSearchResultModel, "BPSearchResultModel");
			// return res.d.results;
			// });

		},
		onPressSearch: function (oEvent) {
			// debugger;
			var searchParameters = this.getView().getModel("filterModel").getProperty("/filterData");
			var recordCount = "100";
			this._getBusinessPartnerMain(searchParameters, recordCount);
		},
		_getBusinessPartnerMain: function (searchFilter, recordCount) {
			var targetURL = "A_BusinessPartnerTaxNumber?$filter=(",
				that = this,
				bPCode = searchFilter.bPCode,
				bPName = searchFilter.bPName,
				vatRegNo = searchFilter.vATRegistrationNo,
				compRegNo = searchFilter.companyRegistrationNo;
			var urlArray = [];
			var len = urlArray.length;
			var vatPromise,
				compPromise,
				codeNamePromise;
			if (vatRegNo) {
				urlArray[len] = [];
				urlArray[len][0] = "BPTaxNumber";
				urlArray[len][1] = vatRegNo;
				targetURL = targetURL + this._createURL(urlArray, " and ") + ")&$format=json";
				vatPromise = new Promise(function (resolve, reject) {
					resolve(LookupService.getBusinessPartnerView(targetURL)
						.then(function (res) {
							return res.d.results;
						}));
				});
			}
			targetURL = "A_BusinessPartnerTaxNumber?$filter=(";
			if (compRegNo) {
				urlArray[len] = [];
				urlArray[len][0] = "BPTaxNumber";
				urlArray[len][1] = compRegNo;
				targetURL = targetURL + this._createURL(urlArray, " and ") + ")&$format=json";
				compPromise = new Promise(function (resolve, reject) {
					resolve(LookupService.getBusinessPartnerView(targetURL)
						.then(function (res) {
							return res.d.results;
						}));
				});
			}

			if (bPCode || bPName) {
				urlArray = [];
				len = urlArray.length;
				if (bPCode) {
					urlArray[len] = [];
					urlArray[len][0] = "BusinessPartner";
					urlArray[len][1] = bPCode;
					len = len + 1;
				}
				if (bPName) {
					urlArray[len] = [];
					urlArray[len][0] = "OrganizationBPName1";
					urlArray[len][1] = bPName;
				}
				targetURL = "A_BusinessPartner?$format=json&$expand=to_BusinessPartnerTax&$top=1000&$filter=(" +
					that._createURL(urlArray, " and ") + ")";
				codeNamePromise = new Promise(function (resolve, reject) {
					resolve(LookupService.getBusinessPartnerView(targetURL)
						.then(function (res) {
							return res.d.results;
						}));
				});
			}
			///***********this will execute if there is value in (code || name)==true and value in (vat || company)==true/false
			if (codeNamePromise) {
				Promise.all([vatPromise, compPromise, codeNamePromise]).then(function (res) {
					var compVatBP = [],
						finalResult = [];
					if (res[0] || res[1]) {
						compVatBP = that._callVatCompServices(res);
					} else {
						///***********this will execute if ther is value in (code || name)==true and no value in (vat || company)==false	
						finalResult = res[2];
					}
					///********compare compvat result output with the code name result
					///***********this will execute if ther is value in (code || name)==true and value in (vat || company)==true	
					if (codeNamePromise && (res[0] || res[1])) {
						for (var k = 0; k < compVatBP.length; k++) {
							for (var l = 0; l < res[2].length; l++) {
								if (compVatBP[k] === res[2][l].BusinessPartner) {
									finalResult[finalResult.length] = res[2][l];
									res[2].splice(l, 1);
									continue;
								}
							}
						}
					}
					finalResult.results = finalResult;
					var BPSearchResultModel = new JSONModel(finalResult);
					BPSearchResultModel.setSizeLimit(recordCount); //searchResult.results.length);
					that.getOwnerComponent().setModel(BPSearchResultModel, "BPSearchResultModel");
				});
			}
			///***********this will execute if ther is value in (code || name)==false and value in (vat || company)==true
			else {
				Promise.all([vatPromise, compPromise]).then(function (res) {
					var compVatBP = [],
						finalResult = [];
					if (res[0] || res[1]) {
						compVatBP = that._callVatCompServices(res);
						urlArray = [];
						for (var i = 0; i < compVatBP.length; i++) {
							urlArray[i] = [];
							urlArray[i][0] = "BusinessPartner";
							urlArray[i][1] = compVatBP[i];
						}
						targetURL = "A_BusinessPartner?$format=json&$expand=to_BusinessPartnerTax&$top=" + recordCount + "&$filter=(" +
							that._createURL(urlArray, " or ") + ")";
						LookupService.getBusinessPartnerView(targetURL)
							.then(function (resp) {
								finalResult = resp.d;
								var BPSearchResultModel = new JSONModel(finalResult);
								BPSearchResultModel.setSizeLimit(recordCount); //searchResult.results.length);
								that.getOwnerComponent().setModel(BPSearchResultModel, "BPSearchResultModel");
								return res.d.results;
							});
					}
				});
			}
		},
		_callVatCompServices: function (res) {
			var compVatBP = [];
			if (res[0] && res[1]) {
				for (var i = 0; i < res[0].length; i++) {
					for (var j = 0; j < res[1].length; j++) {
						if (res[0][i].BusinessPartner === res[1][j].BusinessPartner) {
							compVatBP[compVatBP.length] = res[1][j].BusinessPartner;
							res[1].splice(j, 1);
							continue;
						}
					}
				}
			} else if (res[0]) {
				for (var ii = 0; ii < res[0].length; ii++) {
					compVatBP[compVatBP.length] = res[0][ii].BusinessPartner;
				}
			} else if (res[1]) {
				for (var jj = 0; jj < res[1].length; jj++) {
					compVatBP[compVatBP.length] = res[1][jj].BusinessPartner;
				}
			}
			return compVatBP;
		},

		_createURL: function (urlArray, operation) {
			var URL = "";
			if (urlArray.length > 1) {
				for (var i = 0; i < urlArray.length; i++) {
					URL = URL + "substringof('" + urlArray[i][1] + "'," + urlArray[i][0] + ")";
					if (i < urlArray.length - 1) {
						URL = URL + operation;
					}
				}
			} else {
				URL = "substringof('" + urlArray[0][1] + "'," + urlArray[0][0] + ")";
			}
			return URL;
		},
		_createSearchResultRecord: function (respObject) {
			var searchResultObj = {};
			searchResultObj.code = respObject.BusinessPartner;
			searchResultObj.name = respObject.OrganizationBPName1;
			return searchResultObj;
		},
		onPressClear: function (oEvent) {
			this.getView().getModel("filterModel").setProperty("/filterData", {});
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.SearchBP
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.SearchBP
		 */
		// onAfterRendering: function () {

		// },

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.SearchBP
		 */
		onExit: function () {

		}

	});

});