sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("spar.bp.create.starter.BPCreate_Starter.controller.app", {
		onInit: function () {

		}
	});
});