sap.ui.define([
	// "sap/ui/core/mvc/Controller",
	"spar/bp/create/starter/BPCreate_Starter/controller/BaseController",
	"spar/bp/create/starter/BPCreate_Starter/services/LookupService",
	"spar/bp/create/starter/BPCreate_Starter/services/BusinessPartnerService",
	"spar/bp/create/starter/BPCreate_Starter/services/validation",
	"spar/bp/create/starter/BPCreate_Starter/services/buildingBPService",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/Core",
	"sap/ui/core/library",
	"sap/ui/core/message/Message",
	"sap/m/MessagePopover",
	"sap/m/MessagePopoverItem",
	"sap/ui/core/Fragment"
], function (BaseController, LookupService, PostService, ValidationService, BuildingBPService, JSONModel, Core, coreLibrary, Message,
	MessagePopover,
	MessagePopoverItem,
	Fragment) {
	"use strict";
	// var MessageType = coreLibrary.MessageType;
	return BaseController.extend("spar.bp.create.starter.BPCreate_Starter.controller.ObjectPage", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.ObjectPage
		 */
		onInit: function () {

			var bpData = {};
			// var oStorage = {};
			// bpData.contacts = {};
			bpData.generalData = {};
			bpData.address = {};
			// bpData.bpData = [];
			bpData.company = [];

			// var that = this;
			var modelName = "pModel";
			// var literalModel = this.getView().getModel("literalModel");
			this.getView().setModel(new JSONModel(bpData), modelName);
			this._setDefaultValues();

			/////////////____________Define Different Models__________________//////
			this.getView().setModel(new JSONModel({}), "compModel");

		},
		_setDefaultValues: function () {
			var validationData = {};
			this.getView().setModel(new JSONModel(validationData), "validationModel");
		},
		draftSave: function () {
			// debugger;
			var modelName = "pModel";
			var that = this;
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			oStorage.clear();
			var data = that.getView().getModel(modelName).getProperty("/");
			oStorage.put("BusinessPartner.LocalData", data);
			var oDraftIndicator = that.getView().byId("idDraftIndicator");
			oDraftIndicator.showDraftSaving();
			oDraftIndicator.showDraftSaved();
			oDraftIndicator.clearDraftState();
		},
		_callLookupServices: function (that) {
			var modelName = "pModel";
			LookupService.getCountryList().then(function (res) {
				var countryModel = new JSONModel(res.d);
				countryModel.setSizeLimit(res.d.results.length);
				that.getOwnerComponent().setModel(countryModel, "countryModel");
				///Calling province service after country service
				that.callGetProvinceCodeLookupService(that, "/address/countrySAddress", "provinceSAModel");
				that.callGetProvinceCodeLookupService(that, "/address/countryPAddress", "provincePAModel");
				that.callGetDialingCodeLookupService(that, "/address/countrySAddress", "dialingCodeModel");

			});

			LookupService.getStoreFormatList().then(function (res) {
				var storeFormatModel = new JSONModel(res.d);
				storeFormatModel.setSizeLimit(res.d.results.length);
				that.getOwnerComponent().setModel(storeFormatModel, "storeFormatModel");
			});
			LookupService.getAccountClerkList().then(function (res) {
				var accountClerkModel = new JSONModel(res.d);
				accountClerkModel.setSizeLimit(res.d.results.length);
				that.getOwnerComponent().setModel(accountClerkModel, "accountClerkModel");
			});
			LookupService.getCorporateStoreIndicatorList().then(function (res) {
				var corporateStoreIndicatorModel = new JSONModel(res.d);
				corporateStoreIndicatorModel.setSizeLimit(res.d.results.length);
				that.getOwnerComponent().setModel(corporateStoreIndicatorModel, "corporateStoreIndicatorModel");
			});
			LookupService.getStoreOpenReasonList().then(function (res) {
				var storeOpenReasonModel = new JSONModel(res.d);
				storeOpenReasonModel.setSizeLimit(res.d.results.length);
				that.getOwnerComponent().setModel(storeOpenReasonModel, "storeOpenReasonModel");
			});
			LookupService.getBankNameList().then(function (res) {
				var bankNameModel = new JSONModel(res.d);
				bankNameModel.setSizeLimit(res.d.results.length);
				that.getOwnerComponent().setModel(bankNameModel, "bankNameModel");
			});

			LookupService.getPaymentMethodsList(this.getView().getModel(modelName).getProperty("/address/countrySAddress")).then(function (res) {
				var PaymentMethodModel = new JSONModel(res.d);
				PaymentMethodModel.setSizeLimit(res.d.results.length);
				that.getOwnerComponent().setModel(PaymentMethodModel, "PaymentMethodModel");
			});
			LookupService.getPaymentTermsList().then(function (res) {
				var results = res.d.results;
				var result = results.filter(function (el) {
					if (el.description !== "") {
						return el;
					}
				});
				res.d.results = result;
				var PaymentTermsModel = new JSONModel(res.d);
				PaymentTermsModel.setSizeLimit(res.d.results.length);
				that.getOwnerComponent().setModel(PaymentTermsModel, "PaymentTermsModel");
			});
			////////////////////////////////
			// LookupService.getCompanyCodeList().then(function (res) {
			// 	// debugger;
			// 	var Payment = new JSONModel(res.d);
			// 	// PaymentMethodModel.setSizeLimit(res.d.results.length);
			// 	// that.getOwnerComponent().setModel(PaymentMethodModel, "PaymentMethodModel");
			// });

		},

		onClickAddCompany: function (oEvent) {
			var oCompany = {
				"companyCode": "",
				"reconciliationAccount": "",
				"paymentTerms": "X01S",
				"paymentMethods": "D",
				"userAtCustomer": "",
				"accountingClerkEmails": "",
				"validFields": {
					"companyCode": "None",
					"userAtCustomer": "None",
					"accountingClerkEmails": "None"
				},
				"validationMsg": {
					"companyCode": "",
					"userAtCustomer": "",
					"accountingClerkEmails": ""
				}
			};
			this.addNewRow(oEvent, oCompany);
		},
		_bindingWithFragment: function (idFragment, sPath, oType) {
			this.getView().getModel("pModel").setProperty(sPath, oType);
			this.getView().byId(idFragment).bindElement({
				path: sPath,
				model: "pModel"
			});
		},

		onCountrySAChange: function (oEvent) {
			this.callGetProvinceCodeLookupService(this, "/address/countrySAddress", "provinceSAModel");
			this.callGetDialingCodeLookupService(this, "/address/countrySAddress", "dialingCodeModel");
		},
		onCountryPAChange: function (oEvent) {
			this.callGetProvinceCodeLookupService(this, "/address/countryPAddress", "provincePAModel");
		},
		callGetProvinceCodeLookupService: function (that, propertyURL, modelName) {
			// debugger;
			LookupService.getProvinceCode(this.getView().getModel("pModel").getProperty(propertyURL)).then(function (response) {
				var provinceModel = new JSONModel(response.d);
				provinceModel.setSizeLimit(response.d.results.length);
				that.getOwnerComponent().setModel(provinceModel, modelName);
			});
		},
		callGetDialingCodeLookupService: function (that, propertyURL, modelName) {
			LookupService.getDialingCode(this.getView().getModel("pModel").getProperty(propertyURL)).then(function (response) {
				var dialingCodeModel = new JSONModel(response.d);
				dialingCodeModel.setSizeLimit(response.d.results.length);
				that.getOwnerComponent().setModel(dialingCodeModel, modelName);
			});
		},

		onBankTableAddPress: function (oEvent) {
			var dialingCode = "";
			try {
				dialingCode = this.getView().getModel("dialingCodeModel").getProperty("/").results[0].countryCode;
			} catch (ex) {
				dialingCode = this.getView().getModel("literalModel").getProperty("/").countryCode; //"+27";
			}
			var oBank = {
				"countryBankDetail": dialingCode, //this.getView().getModel("literalModel").getProperty("/").countryCode, //"ZA",
				"bankNameBank": "FIRST NATIONAL BANK",
				"bankCodeBank": "",
				"accountNumberBank": "",
				"accountNameBank": "",
				"bankUsedForBank": "",
				"validFields": {
					"bankCodeBank": "None",
					"accountNumberBank": "None",
					"accountNameBank": "None"
				},
				"validationMsg": {
					"bankCodeBank": "",
					"accountNumberBank": "",
					"accountNameBank": ""
				}
			};
			this.addNewRow(oEvent, oBank);
			var controlId = oEvent.getSource().getParent().getParent();
			this.getView().byId(controlId.getId()).setModel(this.getOwnerComponent().getModel("bankNameModel"), "bankNameModel");
		},
		onEmailAddPress: function (oEvent) {
			var oEmail = {
				"primary": true,
				"emailContactType": "",
				"emailId": "",
				"validFields": {
					"emailId": "None"
				},
				"validationMsg": {
					"emailId": ""
				}
			};
			this.addNewRow(oEvent, oEmail);
		},

		onCellphoneAddPress: function (oEvent) {
			var dialingCode = "";
			try {
				dialingCode = this.getView().getModel("dialingCodeModel").getProperty("/").results[0].dialingCode;
			} catch (ex) {
				dialingCode = this.getView().getModel("literalModel").getProperty("/").countrydialingCode; //"+27";
			}
			var oCellphone = {
				"primary": true,
				"cellphoneContactType": "",
				"cellphoneDialingCode": dialingCode,
				"cellphoneNo": "",
				"cellphoneName": "",
				"validFields": {
					"cellphoneNo": "None"
				},
				"validationMsg": {
					"cellphoneNo": ""
				}
			};
			this.addNewRow(oEvent, oCellphone);
		},

		onFaxAddPress: function (oEvent) {
			var dialingCode = "";
			try {
				dialingCode = this.getView().getModel("dialingCodeModel").getProperty("/").results[0].dialingCode;
			} catch (ex) {
				dialingCode = this.getView().getModel("literalModel").getProperty("/").countrydialingCode; //"+27";
			}

			var oFaxData = {
				"primary": true,
				"faxContactType": "",
				"faxDialingCode": dialingCode,
				"faxNo": "",
				"faxExtension": "",
				"faxName": "",
				"validFields": {
					"faxNo": "None"
				},
				"validationMsg": {
					"faxNo": ""
				}
			};
			this.addNewRow(oEvent, oFaxData);
		},

		onTelephoneTableAddPress: function (oEvent) {
			var dialingCode = "";
			try {
				dialingCode = this.getView().getModel("dialingCodeModel").getProperty("/").results[0].dialingCode;
			} catch (ex) {

				dialingCode = this.getView().getModel("literalModel").getProperty("/").countrydialingCode; //"+27";
			}
			var oTelephoneData = {
				"primary": false,
				"telephoneName": "",
				"idTelephoneTabContactType": "",
				"telephoneDialingCode": dialingCode,
				"telephoneNo": "",
				"telephoneExtension": "",
				"validFields": {
					"telephoneNo": "None"
				},
				"validationMsg": {
					"telephoneNoMsg": ""
				}
			};
			this.addNewRow(oEvent, oTelephoneData);
		},

		onRowDeletePress: function (oEvent) {
			var oPath = oEvent.getSource().getParent().getParent().getBinding().sPath;
			var controlId = oEvent.getSource().getParent().getParent().getId();
			var modelName = "pModel";
			var oModel = this.byId(controlId).getModel(modelName);
			// if (this.byId(controlId).getSelectedIndex() !== -1) {
			var iIndex = oEvent.getSource().getParent().getIndex(); //this.byId(controlId).getSelectedIndices()[0];
			///*******check if the selected one is primary then make the first as primary
			if (oModel.getProperty(oPath)[iIndex].primary === true && oModel.getProperty(oPath).length !==
				1) {
				oModel.getProperty(oPath).splice(iIndex, 1);
				oModel.getProperty(oPath)[0].primary = true;
			} else {
				oModel.getProperty(oPath).splice(iIndex, 1);
			}
			this.byId(controlId).clearSelection();
			oModel.refresh(true);
			// }
		},

		addNewRow: function (oEvent, oData) {
			var modelName = "pModel";
			// var oTelephoneData = oData;
			var oModel = this.getView().getModel(modelName);
			var that = this;
			var oPath = oEvent.getSource().getParent().getParent().getBinding().sPath;
			var controlId = oEvent.getSource().getParent().getParent();
			if (!oModel.getProperty(oPath)) {
				oModel.setProperty(oPath, []);
				oModel.getProperty(oPath).push(oData);
				this.getView().byId(controlId.getId()).setModel(this.getView().getModel(modelName), modelName);
				this.getView().byId(controlId.getId()).getModel(modelName).refresh(true);
			} else {
				var aLength = oModel.getProperty(oPath).length;
				if (aLength === 0) {
					oModel.getProperty(oPath).push(oData);
					this.getView().byId(controlId.getId()).getModel(modelName).refresh(true);
				} else {
					var tableInfo = oEvent.getSource().getParent().getParent();
					var valid = ValidationService.validateOnAddInArrayFields(that, tableInfo);

					if (valid) {
						oData.primary = false;
						// oModel.getProperty(oPath).push(oData);
						oModel.getProperty(oPath).unshift(oData);
						this.getView().byId(controlId.getId()).getModel(modelName).refresh(true);
					}
					// }
				}
			}
		},

		toUpperCaseChange: function (oEvent) {
			var oInput = oEvent.getSource();
			oInput.setValue(oEvent.getSource().getValue().toUpperCase());
		},

		// Function for the SUBMIT button press
		onSubmitPress: function (oEvent) {

			///**************Checking validation
			// var message = "Enter a value with at least 1 characters";
			var submitFlag = true;
			var pModel = "pModel";
			this.getView().getModel("validationModel").setProperty("/", []);

			// this.handelingRequiredField(fieldIds);
			var oView = this.getView();
			var objectPageId = "idObjectPageLayout";
			var fieldIds = [
				oView.byId("idCode"),
				oView.byId("idNameGroupDetail"),
				oView.byId("idEntityRegistrationNumber"),
				oView.byId("idStreetNameSAddress"),
				oView.byId("idCitySAddress"),
				oView.byId("idPostalCodeSAddress")
			];
			submitFlag = ValidationService.checkInputConstraints(this, fieldIds);
			////*****need not to check 2nd validation if mandetory field validation returning false
			if (submitFlag) {
				var arrayIds = [
					oView.byId("idTelephoneTableContactDetail"),
					oView.byId("idFaxTableContactDetail"),
					oView.byId("idCellphoneTableContactDetail"),
					oView.byId("idEmailTableContactDetail"),
					oView.byId("idBankDetail"),
					oView.byId("idCompanyDetail")
				];
				submitFlag = ValidationService.checkInputConstraints(this, arrayIds, true);
			}

			if (submitFlag) { //
				var processData = {};
				var data = this.getView().getModel(pModel).getProperty("/");
				this.SetPageBusy(oEvent);
				processData.actionCreate = "true";
				processData.actionUpdate = "false";
				processData.request = {};
				processData.request.data = {};
				processData.request.inBp = {};
				processData.request.data = BuildingBPService.buildingBPObjectMapping(data);
				processData.request.inBp = this.getView().getModel(pModel).getProperty("/");
				// var processData = {};
				// this.SetPageBusy(oEvent);
				// processData.actionCreate = "true";
				// processData.actionUpdate = "false";
				// processData.data = BuildingBPService.buildingBPObjectMapping();
				// processData.data = this.getView().getModel("pModel").getData();
				// debugger;
				// PostService.postToBusinessProcess(processData);
				// var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
				// oStorage.clear("myLocalData");
				// window.clearInterval(this.getView().getModel("gModel").getProperty("/autoSaveInterval"));
				// debugger;
				// >>> >>> > 330 c09c Field validation
				// for following sections 1. General detail 2. Address Detail

				BuildingBPService.buildingBPToView(this, processData.request.inBp);
				PostService.postToBusinessProcess(processData);
				var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
				oStorage.clear("BusinessPartner.LocalData");
				// window.clearInterval(this.getView().getModel("gModel").getProperty("/autoSaveInterval"));
				// debugger;

				this.getView().getModel(pModel).setProperty("/", {});
				this.getView().getModel(pModel).setProperty("/countryGroupDetail", this.getView().getModel("literalModel").countryCode); //"ZA");
				// this.getView().getModel("pModel").setProperty("/openingReasonGeneralDetail", "3");
				// this.getView().getModel("pModel").setProperty("/corporateStoreIndicatorGeneralDetail", "0001");
				this.getView().getModel(pModel).setProperty("/address", {});

				this.getView().getModel(pModel).getProperty("/company", []);
				this.getView().getModel(pModel).refresh(true);

			} else {
				this.getView().byId(objectPageId).setSelectedSection(this.getView().byId(this.getView().getModel("validationModel").getProperty(
					"/")[0]).getId());
			}

		},
		OnCancelPress: function (oEvent) {
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			oStorage.remove("BusinessPartner.LocalData");

			// window.close();

		},
		SetPageBusy: function (oEvent) {
			var oPanel = this.getView().byId("idMainPage");
			oPanel.setBusy(true);
			// simulate delayed end of operation
			window.setTimeout(function () {
				oPanel.setBusy(false);
			}, 3000);
			window.setTimeout(function () {
				sap.m.MessageToast.show("Work Flow started successfuly");
			}, 3500);
		},

		// onAfterItemAdded: function (oEvent) {},
		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.ObjectPage
		 */
		onBeforeRendering: function () {
			var oTelephoneData = {
				"primary": true,
				"telephoneName": "",
				"idTelephoneTabContactType": "",
				"telephoneDialingCode": this.getView().getModel("literalModel").getProperty("/").countrydialingCode, //"+27",
				"telephoneNo": "",
				"telephoneExtension": "",
				"validFields": {
					"telephoneNo": "None"
				},
				"validationMsg": {
					"telephoneNo": ""
				}
			};
			var oModel = this.getView().getModel("pModel");
			if (!oModel.getProperty("/contacts")) {
				oModel.setProperty("/contacts", {});
				oModel.setProperty("/contacts/telephoneData", []);
				oModel.getProperty("/contacts/telephoneData").push(oTelephoneData);
			}

			/////////////////

			var modelName = "pModel";
			var bpData = this.getView().getModel(modelName).getProperty("/");
			var literalModel = this.getView().getModel("literalModel");
			var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			// oStorage.put("myLocalData", data);
			if (oStorage.get("BusinessPartner.LocalData")) {
				bpData = oStorage.get("BusinessPartner.LocalData");
				if (bpData.generalData.liquorLicenceNumberStartDate) {
					bpData.generalData.liquorLicenceNumberStartDate = new Date(bpData.generalData.liquorLicenceNumberStartDate); //"2020-04-20T09:22:03.092Z"
				}
				if (bpData.generalData.liquorLicenceNumberEndDate) {
					bpData.generalData.liquorLicenceNumberEndDate = new Date(bpData.generalData.liquorLicenceNumberEndDate); //"2020-04-20T09:22:03.092Z"
				}
				if (bpData.generalData.grocerWineLicenceNumberStartDate) {
					bpData.generalData.grocerWineLicenceNumberStartDate = new Date(bpData.generalData.grocerWineLicenceNumberStartDate); //"2020-04-20T09:22:03.092Z"
				}
				// debugger;
				if (bpData.generalData.grocerWineLicenceNumberEndDate) {
					bpData.generalData.grocerWineLicenceNumberEndDate = new Date(bpData.generalData.grocerWineLicenceNumberEndDate); //"2020-04-20T09:22:03.092Z"
				}
				if (bpData.generalData.distributionLicencNumberStartDate) {
					bpData.generalData.distributionLicencNumberStartDate = new Date(bpData.generalData.distributionLicencNumberStartDate); //"2020-04-20T09:22:03.092Z"
				}
				if (bpData.generalData.distributionLicencNumberEndDate) {
					bpData.generalData.distributionLicencNumberEndDate = new Date(bpData.generalData.distributionLicencNumberEndDate); //"2020-04-20T09:22:03.092Z"
				}

			}
			// debugger;
			this.getView().setModel(new JSONModel(bpData), modelName);
			if (!bpData.address.hasOwnProperty("countrySAddress")) {
				// this._bindingWithFragment("idSimpleFormAddressChange", "/address", {});
				this.getView().getModel(modelName).setProperty("/address/countrySAddress", literalModel.getProperty("/").countryCode); //"ZA");
			}
			if (!bpData.address.hasOwnProperty("countryPAddress")) {
				this.getView().getModel(modelName).setProperty("/address/countryPAddress", literalModel.getProperty("/").countryCode); //"ZA");
			}
			if (!bpData.generalData.hasOwnProperty("countryGroupDetail")) {
				this.getView().getModel(modelName).setProperty("/generalData/countryGroupDetail", literalModel.getProperty("/").countryCode); //"ZA");
			}
			///**********Calling services**************///
			this._callLookupServices(this);
			///////////////////
			oModel.refresh();

		}

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.ObjectPage
		 */
		// onAfterRendering: function () {
		// 	debugger;
		// },

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf spar.bp.create.starter.BPCreate_Starter.view.ObjectPage
		 */
		// onExit: function () {
		// 	var data = this.getView().getModel("pModel").getProperty("/");
		// 	var oStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
		// 	oStorage.put("myLocalData", data);
		// }

	});

});